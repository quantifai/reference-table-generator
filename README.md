# Reference Data Plotter


## Dependencies

- To install dependencies run:

```
pipenv install --skip-lock
```

## Usage

```
usage: gen_table.py [-h] [-r REFERENCE] [-w [WIN_RATE [WIN_RATE ...]]]
                    [-t [TRADES [TRADES ...]]] [-s STEP] [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -r REFERENCE, --reference REFERENCE
                        /path/to/reference/data
  -w [WIN_RATE [WIN_RATE ...]], --win-rate [WIN_RATE [WIN_RATE ...]]
                        WR-lower WR-higher
  -t [TRADES [TRADES ...]], --trades [TRADES [TRADES ...]]
                        T-lower T-higher
  -s STEP, --step STEP  Step in number of trades
  -o OUTPUT, --output OUTPUT
```

#!/usr/bin/env python3
# coding: utf-8
"""
    :author: Pratik K
    :brief: Generates Net R table for reference
"""
import argparse
from itertools import product, starmap

import pandas as pd


args = None


def collect_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--reference', type=str, default='reference.csv',
                        help="/path/to/reference/data")
    parser.add_argument('-w', '--win-rate', type=int, nargs="*",
                        help="WR-lower WR-higher", default=[40, 55])
    parser.add_argument('-t', '--trades', type=int, nargs="*",
                        help="T-lower T-higher", default=[400, 800])
    parser.add_argument('-s', '--step', type=int, default=200,
                        help="Step in number of trades")
    parser.add_argument('-o', '--output', type=str, default="lookup.xlsx")
    args = parser.parse_args()
    return args


def main():
    global args
    args = collect_args()
    win_rates = range(args.win_rate[0], args.win_rate[1] + 1)
    n_trades = range(args.trades[0], args.trades[1] + 1, args.step)
    points = product(win_rates, n_trades)
    rows = starmap(lambda w, n: (w, n, int(w * n / 100), int((100 - w) * n / 100)), points)
    del points
    reference = pd.read_csv(args.reference).itertuples(index=False, name=None)
    _df = product(reference, rows)
    netR = lambda ref, stat: (ref[0] * stat[-2] - stat[-1], ref[-1] * stat[-2] - stat[-1])
    Rnet = starmap(netR, _df)
    df = pd.DataFrame([list(x[0]) + list(x[1]) + list(y) for x, y in zip(_df, Rnet)])
    df.columns = ['R', 'spread', 'sl', 'actual_R', 'WR', 'trades', 'wins', 'losses', 'T_netR', 'A_netR']
    df.sort_values(['sl', 'actual_R', 'A_netR', 'WR', 'spread'], inplace=True,
                   ascending=[True, False, False, True, True])
    df.reset_index(inplace=True, drop=True)
    print(df.head())
    print("Dumping data ...")
    df.to_csv('lookup.csv', index=False, float_format="%.2f")
    df.to_excel(args.output)


if __name__ == "__main__":
    main()

